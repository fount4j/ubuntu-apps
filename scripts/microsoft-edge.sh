#!/usr/bin/env bash

set -euo pipefail

active_win="$(xdotool getactivewindow)"
edge_wins="$(xdotool search --class 'Microsoft-edge' || true)"

if [[ -z "$edge_wins" ]]; then
  microsoft-edge
  exit 0
fi

while IFS= read -r win_id; do
  if [[ "$active_win" == "$win_id" ]]; then
    xdotool windowminimize "$win_id"
    exit 0
  fi
done <<<"$edge_wins"

while IFS= read -r win_id; do
  xdotool windowactivate "$win_id" >/dev/null 2>&1
done <<<"$edge_wins"
