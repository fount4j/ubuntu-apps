#!/usr/bin/env bash

set -euo pipefail

source_dir="./fish/functions/$1"
target_dir="$HOME/.config/fish/functions"

mkdir -p "$target_dir"

for file in "$source_dir"/*.fish; do
  if [ -f "$file" ]; then
    filename=$(basename "$file")

    if [ ! -f "$target_dir/$filename" ]; then
      cp "$file" "$target_dir/"
      echo "Copied $filename to $target_dir"
    else
      echo "$filename already exists in $target_dir"
    fi
  fi
done
