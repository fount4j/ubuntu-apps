#!/usr/bin/env bash

set -euo pipefail

# 查找 Alacritty 窗口的 ID
sleep 0.01
WINDOW_ID=($(xdotool search --class 'Alacritty' || true))

if [[ "${#WINDOW_ID[@]}" == "0" ]]; then
  # Alacritty 没有运行，启动它
  alacritty &
  exit 0
fi

# 检查窗口是否已经获得焦点
FOCUSED_WINDOW=$(xdotool getwindowfocus)

if [[ "${WINDOW_ID[0]}" == "$FOCUSED_WINDOW" ]]; then
    # 如果已经获得焦点，则隐藏窗口
    xdotool windowunmap ${WINDOW_ID[0]}
else
    # 如果没有获得焦点，则显示并聚焦窗口
    xdotool windowmap ${WINDOW_ID[0]}
    xdotool windowactivate ${WINDOW_ID[0]}
fi

