function ksns --wraps='kubectl config set-context --current --namespace' --description 'alias ksns=kubectl config set-context --current --namespace'
    kubectl config set-context --current --namespace $argv
end
