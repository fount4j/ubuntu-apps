function kgcm --wraps='kubectl get cm' --description 'alias kgcm=kubectl get cm'
    kubectl get cm $argv
end
