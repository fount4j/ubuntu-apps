function kgd --wraps='kubectl get deploy' --description 'alias kgd=kubectl get deploy'
    kubectl get deploy $argv
end
