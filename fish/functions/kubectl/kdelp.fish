function kdelp --wraps='kubectl delete pod' --description 'alias kdelp=kubectl delete pod'
    kubectl delete pod $argv
end
