function keti --wraps='kubectl exec -it' --description 'alias keti=kubectl exec -it'
    kubectl exec -it $argv
end
