function kgs --wraps='kubectl get service' --description 'alias kgs=kubectl get service'
    kubectl get service $argv
end
