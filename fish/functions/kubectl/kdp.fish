function kdp --wraps='kubectl describe pod' --description 'alias kdp=kubectl describe pod'
    kubectl describe pod $argv
end
