function kgdsa --wraps='kubectl get deamonset -a' --wraps='kubectl get deamonset -A' --wraps='kubectl get daemonsets -A' --description 'alias kgdsa=kubectl get daemonsets -A'
    kubectl get daemonsets -A $argv
end
