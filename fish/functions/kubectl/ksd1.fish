function ksd1 --wraps='kubectl scale --replicas 1 deployment' --description 'alias ksd1=kubectl scale --replicas 1 deployment'
    kubectl scale --replicas 1 deployment $argv
end
