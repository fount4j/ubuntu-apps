function ked --wraps='kubectl edit deploy' --description 'alias ked=kubectl edit deploy'
    kubectl edit deploy $argv
end
