function kgnp --wraps='kubectl get netpol' --description 'alias kgnp=kubectl get netpol'
    kubectl get netpol $argv
end
