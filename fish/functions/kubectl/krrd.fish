function krrd --wraps='kubectl rollout restart deployment' --description 'alias krrd=kubectl rollout restart deployment'
    kubectl rollout restart deployment $argv
end
