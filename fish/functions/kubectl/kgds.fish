function kgds --wraps='kubectl get deamonset' --wraps='kubectl get daemonsets' --description 'alias kgds=kubectl get daemonsets'
    kubectl get daemonsets $argv
end
