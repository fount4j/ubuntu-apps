function kgns --wraps='kubectl get ns' --description 'alias kgns=kubectl get ns'
    kubectl get ns $argv
end
