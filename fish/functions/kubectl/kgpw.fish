function kgpw --wraps='kubectl get pods -w' --description 'alias kgpw=kubectl get pods -w'
    kubectl get pods -w $argv
end
