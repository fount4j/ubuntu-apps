# 我在用的 Ubuntu 软件

> 已更新为 Ubuntu 24.04 版本，旧的 Ubuntu 系统一些脚本可能不好用

## 更新自带程序

```shell
sudo apt update && sudo apt upgrade -y
```

## 基础软件

- [curl](https://github.com/curl/curl): （必装）HTTP 请求工具，后续下载其他应用时会用到。
- [git](https://git-scm.com/about):（必装）版本管理工具，后续安装其他应用时会用到。
- [vim](https://github.com/vim/vim): 命令行编辑器。
- [bat](https://github.com/sharkdp/bat): 预览文件内容。
- [eza](https://github.com/eza-community/eza): ls 替代程序。
- [fd-find](https://github.com/sharkdp/fd): 文件搜索工具。
- [fzf](https://github.com/junegunn/fzf): 交互模式下模糊搜索文件。
- [ripgrep](https://github.com/BurntSushi/ripgrep): 搜索文件内容。
- [ncdu](https://dev.yorhel.nl/ncdu): 磁盘占用分析。
- [jq](https://github.com/jqlang/jq): 命令行 JSON 工具。
- [sshpass](https://github.com/kevinburke/sshpass): SSH 自动输入密码。
- [zoxide](https://github.com/ajeetdsouza/zoxide): 智能化的 cd 命令。

```bash
sudo apt install -y curl git vim bat eza fd-find fzf ripgrep ncdu jq sshpass zoxide
```

## （可选）使用 ghproxy 加速 GitHub 访问

> 安装 Fish 和 Fish 插件的时候建议启用
> 启用该配置后 Neovim 的一些插件可能安装失败

```bash
git config --global url."https://ghproxy.net/https:/github.com/".insteadOf "https://github.com/"
```

使用下面的命令删除该配置

```bash
git config --global --remove-section url."https://ghproxy.net/https:/github.com/"
```

## 克隆项目

```bash
git clone https://gitee.com/fount4j/ubuntu-apps.git
cd ubuntu-apps
```

## --proxy 参数

后续的软件安装命令带有 `--proxy` 参数的，是利用了镜像服务器进行了下载加速。如果希望从 GitHub
或软件官网下载文件，可以去掉 `--proxy` 参数。

### alacritty 终端

```bash
bash ./install/ui/alacritty.sh
```

## Lazygit

```bash
bash ./install/lazygit.sh --proxy
```

## [bottom](https://github.com/ClementTsang/bottom): 资源占用监控

```bash
bash ./install/bottom.sh --proxy
```

## 安装字体

```bash
bash ./install/ui/fonts.sh --proxy
```

## 安装 Fish shell

```bash
bash ./install/fish.sh
```

## Zellij 终端服用工具，tmux 替代品

```bash
bash ./install/zellij.sh --proxy
```

## neovim vim 编辑器

```bash
bash ./install/neovim.sh
```

安装完成后建议输入 `nvim` 命令进入 neovim 自动安装插件。
等到大部分插件安装完成后，如果还剩一两个插件安装失败，执行下面的命令取消代理配置后再进入 `nvim` 下载就好了。

```bash
git config --global --remove-section url."<https://ghproxy.net/https:/github.com/>"
```

## 开发相关

### SDK Manager 管理 Java 环境

```bash
curl -s "https://get.sdkman.io" | bash
```

如果使用的 Fish，需要安装一个 sdkman 的 Fish 插件

```bash
fisher install reitzig/sdkman-for-fish@v2.0.0
```

### Docker

```bash
bash ./install/docker.sh --proxy
```

### Node.js

```bash
bash ./install/nodejs.sh --proxy
```

可选安装 nvm fish 插件

```bash
fisher install jorgebucaran/nvm.fish
```

## 需要 UI 的工具

### 搜狗输入法

```bash
bash ./install/ui/sogou.sh
```

### [uTools](https://u.tools/) 效率工具

```bash
bash ./install/ui/utools.sh
```

### VS Code

```bash
bash ./install/ui/code.sh
```

### Flameshot 截图 & Peek 录 GIF

```bash
sudo apt install -y flameshot peek
```

### [Edge 浏览器](https://www.microsoft.com/zh-cn/edge)

用 Edge 浏览器的原因是 Chrome 内核，而且登录用户同步数据比较方便。

```bash
bash ./install/ui/edge.sh
```

### [Optional: FreeOffice](https://www.freeoffice.com/en/download/applications)

### [Optional: WPS](https://linux.wps.cn/)

### VLC 视频播放器

```bash
sudo apt install -y vlc
```

### GNOME 插件

```bash
bash ./install/ui/gnome-extension.sh
```

- [Dash to Panel](https://extensions.gnome.org/extension/1160/dash-to-panel/) Windows 风格的任务栏
- [Tray Icons: Reloaded](https://extensions.gnome.org/extension/2890/tray-icons-reloaded/) 系统托盘
- [Transparent Window Moving](https://extensions.gnome.org/extension/1446/transparent-window-moving/) 让窗口被拖动时变为半透明
- [Switcher](https://extensions.gnome.org/extension/973/switcher/) 快速切换应用，Alt + Tab 的替代品。
- [Tactile](https://gitlab.com/lundal/tactile) 管理窗口布局

### [非官方版本的 Microsoft Teams Linux 客户端](https://www.microsoft.com/zh-cn/microsoft-teams/group-chat-software/)

```bash
bash ./install/ui/teams.sh --proxy
```

### [k3s](https://k3s.io/) 轻量级 Kubernetes

```bash
bash ./install/k3s.sh --proxy
```

卸载 k3s 使用命令 `bash ./uninstall/k3s.sh`

### [k9s](https://github.com/derailed/k9s)

```bash
curl -sS https://webinstall.dev/k9s | bash
```
