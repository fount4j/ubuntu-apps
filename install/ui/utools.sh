#!/usr/bin/env bash

set -euo pipefail
set -o errtrace

DOWNLOAD_LINK="$(curl -sL https://u.tools/download | grep -Po 'https://[^"]+_amd64.deb')"

echo "Download $DOWNLOAD_LINK"
trap 'rm -f "$TMPFILE"' EXIT
TMPFILE=$(mktemp) || exit 1
curl -L "$DOWNLOAD_LINK" -o "$TMPFILE"

sudo dpkg -i "$TMPFILE"
sudo apt install -f -y

