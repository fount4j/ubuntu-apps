#!/usr/bin/env bash

set -euo pipefail

sudo apt install -y alacritty xdotool

mkdir -p ~/.config/alacritty
cp ./config/alacritty/alacritty.toml ~/.config/alacritty/alacritty.toml

mkdir -p ~/.local/bin
cp ./scripts/alacritty.sh ~/.local/bin/
chmod +x ~/.local/bin/alacritty.sh

source ./scripts/setup-hotkey.sh alacritty "$HOME/.local/bin/alacritty.sh" 'F1'
