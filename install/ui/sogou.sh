#!/usr/bin/env bash

set -euo pipefail

sudo apt install -y fcitx libqt5qml5 libqt5quick5 libqt5quickwidgets5 qml-module-qtquick2 libgsettings-qt1

echo "Please download and install sogou from this page"
xdg-open "https://shurufa.sogou.com/linux"
