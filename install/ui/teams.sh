#!/usr/bin/env

set -euo pipefail

DOWNLOAD_LINK="$(curl -sSL https://api.github.com/repos/IsmaelMartinez/teams-for-linux/releases/latest | grep -Po '"browser_download_url": "\K[^"]+_amd64[^"]*\.deb')"

trap 'rm -f "$TMPFILE"' EXIT
TMPFILE=$(mktemp) || exit 1

if [[ "${1:-}" == '--proxy' ]]; then
  DOWNLOAD_LINK="https://ghproxy.net/$DOWNLOAD_LINK"
fi

echo "Download Teams from $DOWNLOAD_LINK"
curl -L -o "$TMPFILE" "$DOWNLOAD_LINK"
sudo dpkg -i "$TMPFILE"
sudo apt install -f -y

source ./scripts/setup-hotkey.sh teams teams-for-linux '<Control><Alt>z'
