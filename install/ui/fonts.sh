#!/usr/bin/env bash

set -euo pipefail

mkdir -p ~/.local/share/fonts

trap 'rm -f "$TMPFILE"' EXIT
TMPFILE=$(mktemp) || exit 1

trap 'rm -rf "$TMPDIR"' EXIT
TMPDIR=$(mktemp -d) || exit 1

APP_VERSION="$(curl -sSL https://api.github.com/repos/ryanoasis/nerd-fonts/releases/latest | grep -Po '"tag_name": "\K[^"]*')"
DOWNLOAD_LINK="https://github.com/ryanoasis/nerd-fonts/releases/download/${APP_VERSION}/CascadiaMono.zip"

proxy_prefix=''
if [[ "${1:-}" == "--proxy" ]]; then
  proxy_prefix='https://ghproxy.net/'
  DOWNLOAD_LINK="$proxy_prefix$DOWNLOAD_LINK"
fi

wget -O "$TMPFILE" "$DOWNLOAD_LINK"
unzip "$TMPFILE" -d "$TMPDIR"
cp "$TMPDIR"/*.ttf ~/.local/share/fonts

trap 'rm -f "$TMPFILE2"' EXIT
TMPFILE2=$(mktemp) || exit 1

trap 'rm -rf "$TMPDIR2"' EXIT
TMPDIR2=$(mktemp -d) || exit 1

wget -O "$TMPFILE2" "${proxy_prefix}https://github.com/iaolo/iA-Fonts/archive/refs/heads/master.zip"
unzip "$TMPFILE2" -d "$TMPDIR2"
cp "$TMPDIR2"/iA-Fonts-master/iA\ Writer\ Mono/Static/iAWriterMonoS-*.ttf ~/.local/share/fonts

fc-cache
