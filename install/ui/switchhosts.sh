#!/usr/bin/env

set -euo pipefail

DOWNLOAD_LINK="$(curl -sSL https://api.github.com/repos/oldj/SwitchHosts/releases/latest | grep -Po '"browser_download_url": "\K[^"]+_amd64[^"]+\.deb')"

trap 'rm -f "$TMPFILE"' EXIT
TMPFILE=$(mktemp) || exit 1

if [[ "${1:-}" == '--proxy' ]]; then
  DOWNLOAD_LINK="https://ghproxy.net/$DOWNLOAD_LINK"
fi

echo "Download SwitchHosts from $DOWNLOAD_LINK"
curl -L -o "$TMPFILE" "$DOWNLOAD_LINK"
sudo apt install --fix-broken "$TMPFILE"

mkdir -p ~/.SwitchHosts/data/list/
cp ./config/switchhosts/tree.json ~/.SwitchHosts/data/list/
