#!/usr/bin/env bash

set -euo pipefail

sudo apt install -y flameshot

source ./scripts/setup-hotkey.sh flameshot 'flameshot gui' '<Control><Alt>a'
