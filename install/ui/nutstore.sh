#!/usr/bin/env bash

set -euo pipefail

trap 'rm -f "$TMPFILE"' EXIT
TMPFILE=$(mktemp) || exit 1

if [[ "$XDG_CURRENT_DESKTOP" == "KDE" ]]; then
  echo "检测到 KDE 环境，将通过源代码安装坚果云"
  sudo apt install -y python3-gi gir1.2-appindicator3-0.1 gir1.2-notify-0.7
  wget https://www.jianguoyun.com/static/exe/installer/nutstore_linux_dist_x64.tar.gz -O "$TMPFILE"
  mkdir -p ~/.nutstore/dist && tar zxf "$TMPFILE" -C ~/.nutstore/dist
  ~/.nutstore/dist/bin/install_core.sh
else
  echo "将通过 deb 安装包安装坚果云"
  wget https://pkg-cdn.jianguoyun.com/static/exe/installer/gnome-43/nautilus_nutstore_amd64.deb -O "$TMPFILE"
  sudo dpkg -i "$TMPFILE" || true
  sudo apt install -f -y
fi
