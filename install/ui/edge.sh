#!/usr/bin/env bash

set -euo pipefail

trap 'rm -f "$TMPFILE"' EXIT
TMPFILE=$(mktemp) || exit 1

echo "Downloading microsoft-edge to $TMPFILE"
curl -L 'https://go.microsoft.com/fwlink?linkid=2149051&brand=M102' -o "$TMPFILE"
ls -alh "$TMPFILE"
sudo dpkg -i "$TMPFILE"
sudo apt install -f -y

mkdir -p ~/.local/bin
cp ./scripts/microsoft-edge.sh ~/.local/bin/
chmod +x ~/.local/bin/microsoft-edge.sh

source ./scripts/setup-hotkey.sh browser "$HOME/.local/bin/microsoft-edge.sh" 'F2'
