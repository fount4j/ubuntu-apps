#!/usr/bin/env bash

set -euo pipefail

sudo apt install -y peek

source ./scripts/setup-hotkey.sh peek peek '<Control><Alt>g'
