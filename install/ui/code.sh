#!/usr/bin/env bash
set -euo pipefail

trap 'rm -f "$TMPFILE"' EXIT
TMPFILE=$(mktemp) || exit 1

wget -O "$TMPFILE" 'https://code.visualstudio.com/sha/download?build=stable&os=linux-deb-x64'
sudo dpkg -i "$TMPFILE"

if [[ "${1:-}" != "--without-settings" ]]; then
  mkdir -p ~/.config/Code/User
  cp ./config/code/settings.json ~/.config/Code/User/settings.json
fi
