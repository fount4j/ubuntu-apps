#!/usr/bin/env bash

set -euo pipefail

sudo apt install -y gnome-shell-extension-manager pipx
pipx install gnome-extensions-cli --system-site-packages

# Make it easy to maximize like you can fill left/right
gsettings set org.gnome.desktop.wm.keybindings maximize "['<Super>Up']"
gsettings set org.gnome.desktop.wm.keybindings unmaximize "['<Super>Down']"

# Full-screen with title/navigation bar
gsettings set org.gnome.desktop.wm.keybindings toggle-fullscreen "['<Shift>F11']"

# Turn off default Ubuntu extensions
gnome-extensions disable tiling-assistant@ubuntu.com
gnome-extensions disable ubuntu-appindicators@ubuntu.com
gnome-extensions disable ding@rastersoft.com

# Turn off ubuntu-dock
gsettings set org.gnome.shell.extensions.dash-to-dock hot-keys false
gsettings set org.gnome.shell.extensions.dash-to-dock hotkeys-overlay false
gsettings set org.gnome.shell.extensions.dash-to-dock hotkeys-show-dock false
gnome-extensions disable ubuntu-dock@ubuntu.com

# Center new windows in the middle of the screen
gsettings set org.gnome.mutter center-new-windows true
# Set Cascadia Mono as the default monospace font
gsettings set org.gnome.desktop.interface monospace-font-name 'CaskaydiaMono Nerd Font 10'
# Reveal week numbers in the Gnome calendar
gsettings set org.gnome.desktop.calendar show-weekdate true

# Install Dash To Panel
~/.local/bin/gext install dash-to-panel@jderose9.github.com
sudo cp ~/.local/share/gnome-shell/extensions/dash-to-panel@jderose9.github.com/schemas/org.gnome.shell.extensions.dash-to-panel.gschema.xml /usr/share/glib-2.0/schemas/
sudo glib-compile-schemas /usr/share/glib-2.0/schemas/
gsettings set org.gnome.shell.extensions.dash-to-panel appicon-margin 4
gsettings set org.gnome.shell.extensions.dash-to-panel appicon-padding 4
gsettings set org.gnome.shell.extensions.dash-to-panel dot-style-focused 'METRO'
gsettings set org.gnome.shell.extensions.dash-to-panel dot-style-unfocused 'DOTS'
gsettings set org.gnome.shell.extensions.dash-to-panel focus-highlight-dominant false
gsettings set org.gnome.shell.extensions.dash-to-panel focus-highlight-opacity 15
gsettings set org.gnome.shell.extensions.dash-to-panel hot-keys false
gsettings set org.gnome.shell.extensions.dash-to-panel isolate-monitors false
gsettings set org.gnome.shell.extensions.dash-to-panel panel-size 48
gsettings set org.gnome.shell.extensions.dash-to-panel panel-sizes '{"0":32}'
gsettings set org.gnome.shell.extensions.dash-to-panel primary-monitor 0
gsettings set org.gnome.shell.extensions.dash-to-panel trans-panel-opacity 0.2
gsettings set org.gnome.shell.extensions.dash-to-panel trans-use-custom-opacity true
gsettings set org.gnome.shell.extensions.dash-to-panel trans-use-dynamic-opacity false

# Install try icon reloaded
~/.local/bin/gext install trayIconsReloaded@selfmade.pl
sudo cp ~/.local/share/gnome-shell/extensions/trayIconsReloaded@selfmade.pl/schemas/org.gnome.shell.extensions.trayIconsReloaded.gschema.xml /usr/share/glib-2.0/schemas/
sudo glib-compile-schemas /usr/share/glib-2.0/schemas/
gsettings set org.gnome.shell.extensions.trayIconsReloaded icon-margin-horizontal 0
gsettings set org.gnome.shell.extensions.trayIconsReloaded icon-margin-vertical 0
gsettings set org.gnome.shell.extensions.trayIconsReloaded icon-padding-horizontal 0
gsettings set org.gnome.shell.extensions.trayIconsReloaded icon-padding-vertical 0
gsettings set org.gnome.shell.extensions.trayIconsReloaded icon-saturation 0
gsettings set org.gnome.shell.extensions.trayIconsReloaded icon-size 32
gsettings set org.gnome.shell.extensions.trayIconsReloaded icons-limit 8
gsettings set org.gnome.shell.extensions.trayIconsReloaded position-weight 0
gsettings set org.gnome.shell.extensions.trayIconsReloaded tray-margin-left 0
gsettings set org.gnome.shell.extensions.trayIconsReloaded tray-margin-right 0

# Install transparent window moving
~/.local/bin/gext install transparent-window-moving@noobsai.github.com

# Install window switcher
~/.local/bin/gext install switcher@landau.fi
sudo cp ~/.local/share/gnome-shell/extensions/switcher@landau.fi/schemas/org.gnome.shell.extensions.switcher.gschema.xml /usr/share/glib-2.0/schemas/
sudo glib-compile-schemas /usr/share/glib-2.0/schemas/
gsettings set org.gnome.shell.extensions.switcher font-size 18
gsettings set org.gnome.shell.extensions.switcher icon-size 18
gsettings set org.gnome.shell.extensions.switcher max-width-percentage 65
gsettings set org.gnome.shell.extensions.switcher activate-by-key 2

# Install Tactile
~/.local/bin/gext install tactile@lundal.io
sudo cp ~/.local/share/gnome-shell/extensions/tactile@lundal.io/schemas/org.gnome.shell.extensions.tactile.gschema.xml /usr/share/glib-2.0/schemas/
sudo glib-compile-schemas /usr/share/glib-2.0/schemas/
# Configure Tactile
gsettings set org.gnome.shell.extensions.tactile col-0 1
gsettings set org.gnome.shell.extensions.tactile col-1 1
gsettings set org.gnome.shell.extensions.tactile col-2 1
gsettings set org.gnome.shell.extensions.tactile col-3 1
gsettings set org.gnome.shell.extensions.tactile row-0 1
gsettings set org.gnome.shell.extensions.tactile row-1 1
gsettings set org.gnome.shell.extensions.tactile gap-size 0
