#!/usr/bin/env bash

set -euo pipefail

sudo apt install -y neovim build-essential

if [ ! -d "$HOME/.config/nvim" ]; then
  git clone https://github.com/LazyVim/starter ~/.config/nvim
  # Disable update notification popup in starter config
  sed -i 's/checker = { enabled = true }/checker = { enabled = true, notify = false }/g' ~/.config/nvim/lua/config/lazy.lua

  # Enable default extras
  cat <<EOF >~/.config/nvim/lazyvim.json
{
  "extras": [
    "lazyvim.plugins.extras.coding.mini-surround",
    "lazyvim.plugins.extras.dap.core",
    "lazyvim.plugins.extras.lang.docker",
    "lazyvim.plugins.extras.lang.java",
    "lazyvim.plugins.extras.lang.json",
    "lazyvim.plugins.extras.lang.markdown",
    "lazyvim.plugins.extras.lang.rust",
    "lazyvim.plugins.extras.lang.toml",
    "lazyvim.plugins.extras.lang.typescript",
    "lazyvim.plugins.extras.lang.yaml",
    "lazyvim.plugins.extras.util.rest"
  ],
  "news": {
    "NEWS.md": "6077"
  },
  "version": 6
}
EOF

  # Config java plugin
  mkdir -p ~/.config/nvim/lua/plugins/java
  cat <<EOF >~/.config/nvim/lua/plugins/java/init.lua
return {
  {

    "nvim-java/nvim-java",
    dependencies = {
      "nvim-java/lua-async-await",
      "nvim-java/nvim-java-core",
      "nvim-java/nvim-java-test",
      "nvim-java/nvim-java-dap",
      "MunifTanjim/nui.nvim",
      "neovim/nvim-lspconfig",
      "mfussenegger/nvim-dap",
      {
        "williamboman/mason.nvim",
        opts = {
          registries = {
            "github:nvim-java/mason-registry",
            "github:mason-org/mason-registry",
          },
        },
      },
    },
  },
}
EOF
fi
