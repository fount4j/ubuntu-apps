#!/usr/bin/env bash

set -euo pipefail

sudo apt-add-repository ppa:fish-shell/release-3 && sudo apt update && sudo apt install -y fish
sudo usermod -s "$(which fish)" "$USER"

fisher_link="https://raw.githubusercontent.com/jorgebucaran/fisher/main/functions/fisher.fish"
if [[ "${1:-}" == '--proxy' ]]; then
  fisher_link="https://ghproxy.net/$fisher_link"
fi

fish -c "
# fish 的插件管理工具
curl -sL $fisher_link | source && fisher install jorgebucaran/fisher

# 命令行提示
fisher install IlanCosman/tide@v6
# 执行时间较长的命令，结束后发送通知
fisher install franciscolourenco/done
if which zoxide >/dev/null; fisher install kidonng/zoxide.fish; end
if which eza >/dev/null; fisher install lumynou5/eza.fish; end
"

source ./scripts/setup-fish-functions.sh fish
