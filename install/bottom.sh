#!/usr/bin/env bash

echo "Getting download link from GitHub release"
APP_VERSION="$(curl -sSL https://api.github.com/repos/ClementTsang/bottom/releases/latest | grep -Po '"tag_name": "\K[^"]*')"
DOWNLOAD_LINK="https://github.com/ClementTsang/bottom/releases/download/${APP_VERSION}/bottom_${APP_VERSION}_amd64.deb"
echo "Download bottom from $DOWNLOAD_LINK"

trap 'rm -f "$TMPFILE"' EXIT
TMPFILE=$(mktemp) || exit 1

if [[ "${1:-}" == '--proxy' ]]; then
  DOWNLOAD_LINK="https://ghproxy.net/$DOWNLOAD_LINK"
fi

curl -L -o "$TMPFILE" "$DOWNLOAD_LINK"
sudo dpkg -i "$TMPFILE"