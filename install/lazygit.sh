#!/usr/bin/env bash

echo "Getting download link from GitHub release"
APP_VERSION="$(curl -sSL https://api.github.com/repos/jesseduffield/lazygit/releases/latest | grep -Po '"tag_name": "v\K[^"]*')"
DOWNLOAD_LINK="https://github.com/jesseduffield/lazygit/releases/download/v$APP_VERSION/lazygit_${APP_VERSION}_Linux_x86_64.tar.gz"
if [[ "${1:-}" == '--proxy' ]]; then
  DOWNLOAD_LINK="https://ghproxy.net/$DOWNLOAD_LINK"
fi
echo "Download bottom from $DOWNLOAD_LINK"

trap 'rm -rf "$TMPDIR"'
TMPDIR=$(mktemp -d) || exit 1

curl -L -o "$TMPDIR/lazygit.tar.gz" "$DOWNLOAD_LINK"
tar xf "$TMPDIR/lazygit.tar.gz" -C "$TMPDIR"
sudo install "$TMPDIR/lazygit" /usr/local/bin/lazygit

