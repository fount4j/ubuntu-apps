#!/usr/bin/env bash

set -euo pipefail
set -o errtrace

NVM_LINK=https://raw.githubusercontent.com/nvm-sh/nvm/v0.40.1/install.sh
if [[ "${1:-}" == '--proxy' ]]; then
  NVM_LINK="https://ghproxy.net/$NVM_LINK"
fi

curl -o- "$NVM_LINK" | bash

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"
nvm install 22

corepack enable pnpm
