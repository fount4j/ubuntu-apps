#!/usr/bin/env bash

set -euo pipefail

if [[ "${1:-}" == '--proxy' ]]; then
  curl -sfL https://rancher-mirror.rancher.cn/k3s/k3s-install.sh | INSTALL_K3S_MIRROR=cn sh -s - --write-kubeconfig-mode=0644 --system-default-registry=registry.cn-hangzhou.aliyuncs.com
else
  curl -sfL https://get.k3s.io | sh -s - --write-kubeconfig-mode=0644
fi

if which fish >/dev/null; then
  fish -c "if fisher --help >/dev/null; fisher install evanlucas/fish-kubectl-completions; end"
fi

source ./scripts/setup-fish-functions.sh kubectl
