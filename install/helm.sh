#!/usr/bin/env bash

set -euo pipefail

if [[ "${1:-}" == '--proxy' ]]; then
  curl -sfL 'https://ghproxy.net/https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3' | bash
else
  curl -sfL 'https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3' | bash
fi
