#!/usr/bin/env bash

set -euo pipefail

sudo apt install -y ca-certificates gnupg lsb-release
sudo install -m 0755 -d /etc/apt/keyrings

trap 'rm -f "$TMPFILE"' EXIT
TMPFILE=$(mktemp) || exit 1

gpg_link='https://download.docker.com/linux/ubuntu/gpg'
download_link='https://download.docker.com/linux/ubuntu'
if [[ "${1:-}" == "--proxy" ]]; then
  gpg_link='http://mirrors.aliyun.com/docker-ce/linux/ubuntu/gpg'
  download_link='http://mirrors.aliyun.com/docker-ce/linux/ubuntu'
fi
sudo curl -fsSL "$gpg_link" -o /etc/apt/keyrings/docker.asc
sudo chmod a+r /etc/apt/keyrings/docker.asc
echo "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] $download_link $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | sudo tee /etc/apt/sources.list.d/docker.list >/dev/null

sudo apt update

# Install Docker engine and standard plugins
sudo apt install -y docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin docker-ce-rootless-extras

# Give this user privileged Docker access
sudo usermod -aG docker "${USER}"

# Use local logging driver - it's more efficient and uses compression by default.
echo '{"log-driver":"local","log-opts":{"max-size":"10m","max-file":"5"}}' | sudo tee /etc/docker/daemon.json >/dev/null

if which fish >/dev/null; then
  fish -c "if fisher --help >/dev/null; fisher install brgmnn/fish-docker-compose; end"
fi
